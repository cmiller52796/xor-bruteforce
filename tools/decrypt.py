#!/usr/bin/python3
# Run using python3.6+

def is_restricted_alnum(char):
    # 127 is end of the normal ASCII table, this way we can cut out
    # extended utf 8 set
    return char.isalnum() and ord(char) <= 127


def read_file_info(input_file_path):
    with open(input_file_path, 'r') as fileObj:
        file_line = fileObj.read()
        str_len_int = int(file_line.split("\n")[0])
        encrypted_hex_str = file_line.split("\n")[1].strip()
    return str_len_int, encrypted_hex_str


def pad_string_and_listify(encrypted_hex_str):
    encrypted_hex_str_list = encrypted_hex_str.split()
    # Pad string to make it divisible by 6
    while len(encrypted_hex_str_list) % 6 != 0:
        encrypted_hex_str_list.append("00")
    return encrypted_hex_str_list


def chunk_hex_by_six(encrypted_hex_str_list):
    chunkCount = len(encrypted_hex_str_list) // 6
    chunksOfSix = [[0, 0, 0, 0, 0, 0] for _ in range(chunkCount)]
    chunkIteration = 0
    # Chunk hex string into 6, so outerIdx is chunk# with 0-5 being each character
    for i in range(0, chunkCount):
        chunksOfSix[i][0] = int(encrypted_hex_str_list[0 + chunkIteration], 16)
        chunksOfSix[i][1] = int(encrypted_hex_str_list[1 + chunkIteration], 16)
        chunksOfSix[i][2] = int(encrypted_hex_str_list[2 + chunkIteration], 16)
        chunksOfSix[i][3] = int(encrypted_hex_str_list[3 + chunkIteration], 16)
        chunksOfSix[i][4] = int(encrypted_hex_str_list[4 + chunkIteration], 16)
        chunksOfSix[i][5] = int(encrypted_hex_str_list[5 + chunkIteration], 16)
        chunkIteration += 6
    return chunksOfSix


def enumerate_keys(chunked_list):
    # Brute force initial key set from first chunk only
    key_options = [set() for _ in range(6)]
    for possible_key_val in range(0, 256):
        for idx, val in enumerate(chunked_list[0], start=0):
            char = chr(possible_key_val ^ val)
            if not (is_restricted_alnum(char) or char in [".", ",", " "]):
                continue
            key_options[idx].add(possible_key_val)
    # Iterate through the rest of the chunks referencing only the possible
    # key values
    key_options_to_remove = set()
    for chunk in chunked_list[1:]:
        for idx, val in enumerate(chunk, start=0):
            for possibleKeyValue in key_options[idx]:
                char = chr(possibleKeyValue ^ val)
                if not (is_restricted_alnum(char) or char in [".", ",", " "]):
                    key_options_to_remove.add(possibleKeyValue)
            key_options[idx] = key_options[idx] - key_options_to_remove
            key_options_to_remove.clear()
    return key_options


def try_every_key_combination(possible_keys, hex_str_as_list):
    possibilities_dict = {}
    for first in possible_keys[0]:
        for second in possible_keys[1]:
            for third in possible_keys[2]:
                for fourth in possible_keys[3]:
                    for fifth in possible_keys[4]:
                        for sixth in possible_keys[5]:
                            possible_solve_str = ""
                            for idx, hex_str in enumerate(hex_str_as_list, start=0):
                                if hex_str == "00":
                                    continue
                                mod = idx % 6
                                if mod == 0:
                                    possible_solve_str += chr(first ^ int(hex_str, 16))
                                elif mod == 1:
                                    possible_solve_str += chr(second ^ int(hex_str, 16))
                                elif mod == 2:
                                    possible_solve_str += chr(third ^ int(hex_str, 16))
                                elif mod == 3:
                                    possible_solve_str += chr(fourth ^ int(hex_str, 16))
                                elif mod == 4:
                                    possible_solve_str += chr(fifth ^ int(hex_str, 16))
                                elif mod == 5:
                                    possible_solve_str += chr(sixth ^ int(hex_str, 16))
                            possible_key = str(chr(first) + chr(second) + chr(third) + chr(fourth) + chr(fifth))
                            possibilities_dict[possible_key] = possible_solve_str
    return possibilities_dict


def output_to_file(possibility_dict):
    with open("output.txt", 'w') as fileObj:
        for count, possibility in enumerate(possibility_dict, start=1):
            fileObj.write("Attempt: " + str(count) + "\n")
            fileObj.write("Key: " + possibility + "\n")
            fileObj.write(possibility_dict[possibility])
        fileObj.write("\n\n\n")


if __name__ == "__main__":
    stringLengthInt, hexStr = read_file_info("input.txt")
    hexStrList = pad_string_and_listify(hexStr)
    sixHexChunkedList = chunk_hex_by_six(hexStrList)
    possibleKeys = enumerate_keys(sixHexChunkedList)
    possibilityDictionary = try_every_key_combination(possibleKeys, hexStrList)
    output_to_file(possibilityDictionary)
