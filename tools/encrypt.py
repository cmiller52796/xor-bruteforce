#!/usr/bin/python3
# Run using python3.8+

import codecs
import binascii
import sys


def input_checks(key_str, str_to_encrypt):
   if len(key_str) != 6:
      print("ERROR: Key length is invalid, must be 6!")
      sys.exit(1)

   for char in str_to_encrypt:
      if not (char.isalnum() or char in [".", ",", " "]):
         print("ERROR: Character was not an alphanumeric or period or comma!")
         sys.exit(2)


def encrypt_data_to_file(key_str, str_to_encrypt, output_file_path):
   with open(output_file_path, "w") as outputFile:
      lenEncrypt = len(str_to_encrypt)
      encryptedStr = bytearray(lenEncrypt)
      lenKey = len(key_str)
      outputFile.write(str(lenEncrypt) + "\n")

      for idx in range(lenEncrypt):
         keyIdx = idx % lenKey
         encryptedStr[idx] = ord(key_str[keyIdx]) ^ ord(str_to_encrypt[idx])
         outputFile.write(format(encryptedStr[idx], "02x") + " ")
      outputFile.write("\n\n\n")
   print("Key is:", key_str)
   print("Encrypted string is", binascii.hexlify(encryptedStr).hex(' '))


if __name__ == "__main__":
   xorKey = "abcdef"
   encryptThis = "That is how dad did it, that is how America does it, and it has worked out pretty well so far."
   input_checks(xorKey, encryptThis)
   encrypt_data_to_file(xorKey, encryptThis, "input.txt")
