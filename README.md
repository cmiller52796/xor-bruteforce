# xor-bruteforce
A simple brute-force demo assignment for intro level learning. 

## Building
`make build`  
This will pull down a cpp-dev-env image and build the binary.  

## Testing
`make test`  
This will pull down a cpp-dev-env image and run the unit tests.  

