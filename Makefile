DEV_IMAGE=cmiller/cpp-dev-env:$(shell cat cpp-dev-env/VERSION)

.PHONY: all build test clean
all: clean build test

build:
	@docker run --rm -v $(PWD):/workspace $(DEV_IMAGE) \
	/bin/bash -c "cmake -S . -B build && cmake --build build" 

test: 
	@docker run -v $(PWD):/workspace -e GTEST_COLOR=1 $(DEV_IMAGE) \
	/bin/bash -c "ctest -V --test-dir build"

clean:
	@rm -rf build/ CMakeFiles/ Testing/
