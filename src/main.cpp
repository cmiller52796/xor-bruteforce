#include <iostream>
#include <chrono>
#include <thread>
#include "Decrypt.hpp"

using namespace std::chrono_literals;

int main() {
   Decrypt decrypt;
   decrypt.run();
   return 0;
}
